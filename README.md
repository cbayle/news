My litte news page:
* The Raspberry Pixel distribution available for x86 https://www.raspberrypi.org/blog/pixel-pc-mac/
* Gitlab
	* Gitlab core Team https://about.gitlab.com/core-team/
	* Promising GitLab License Finder https://gitlab.com/gitlab-org/gitlab-ee/issues/1125
	* Framagit https://framablog.org/2016/04/19/notre-gitlab-evolue-en-framagit-cest-tres-efficace/
	* Add GitLab Pages to CE https://gitlab.com/gitlab-org/gitlab-ce/issues/14605
		* Done https://about.gitlab.com/2016/12/24/were-bringing-gitlab-pages-to-community-edition/
		* Note the comment "Bring Letsencrypt Support for GitLab Pages" from @c-schmitt
* Forges
	* Gitlab in Debian https://packages.debian.org/search?keywords=gitlab Yeh!
        * Gitlab in Debian https://packages.qa.debian.org/g/gitlab.html Yeh(bis)!
	* Jira VS Tuleap https://blog.enalean.com/comparison-jira-tuleap/
	* Tuleap versus GitLab https://www.tuleap.org/tuleap-versus-gitlab-battle-will-not-happen#comments
		* Need to integrate GitLab with tuleap https://tuleap.net/plugins/forumml/message.php?group_id=101&topic=34607&list=1
* Etherpad
	* http://etherpad.org/
	* http://framacloud.org/cultiver-son-jardin/installation-detherpad/
	* PULL Requests: https://github.com/ether/etherpad-lite/pulls
	* Debian RFP: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=576998
	* MyPads https://philippe.scoffoni.net/installation-mypads-debian-8/
	
* Markdown
	* CSS to make HTML markup look like plain-text markdown http://mrcoles.com/demo/markdown-css/

* IPV6
	* On a LiveBox https://assistance.orange.fr/livebox-modem/toutes-les-livebox-et-modems/installer-et-utiliser/mesurer-votre-debit/ipv6-chez-orange_161850-528413
	* Test your IPv6 connectivity http://test-ipv6.com/

* Curl
	* curl http://wttr.in  # Get local weather forecast in your terminal
	* curl http://v2.wttr.in  # Get local daily graph of temp/precipitation in your terminal
	* curl http://rate.sx/btc
	* curl http://cheat.sh
	* curl http://qrenco.de
